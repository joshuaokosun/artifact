terraform {
    backend "remote" {
        hostname = "app.terraform.io"
        organization = "Arm21"

        workspaces {
            name = "josh"
        }
    }
}